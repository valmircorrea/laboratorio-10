/**
* @file	    main.cpp
* @brief	Arquivo com a função principal do programa.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	19/06/1017
* @date	    20/06/1017
*/

#include <iostream>
using std:: endl;
using std:: cout;

#include <new>
using std::bad_alloc;

#include <cstdlib>
#include <ctime>

#include "valmir.h"

using namespace edb1;

/**
* @brief Função principal do programa.
*/
int main() {

    // Testando a lista duplamente ligada:
    cout << "Teste da LISTA DUPLAMENTE LIGADA:" << endl;

    List<int> *list;

        try {
             list = new List<int>;
        } 
        catch (bad_alloc &ex) {
            cerr << ex.what () << " Erro de alocação dinâmica de memoria para a lista!" << endl;
            exit (1);
        }
        catch (...) {
            cerr << " Erro de alocação de memoria desconhecido!" <<  endl;
            exit (1);
        }
    
    list->insertElement (1);
    list->insertElement (2);
    list->insertElement (3);
    list->insertElement (4);
    list->insertElement (5);

    list->printElements();

    cout << endl;

    // Testando a pilha:
    cout << "Teste da PILHA:" << endl;
    Stack<int> pilha (5);
    
    pilha.push (1);
    pilha.push (2);
    pilha.push (3);
    pilha.push (4);
    pilha.push (5);

   while( !pilha.empty () ) {
        cout << pilha.top () << endl;
        pilha.pop ();
    }

    cout << endl;
        
    // Testanto a fila:
    cout << "Teste da FILA:" << endl;
    Fila<int> fila (5);
    
    fila.push (1);
    fila.push (2);
    fila.push (3);
    fila.push (4);
    fila.push (5);
    
    while( !fila.empty() ) {
        cout << fila.pop() << endl;
    }

    cout << endl;

    //Testa as ordenações:
    srand(time(NULL));
    
    int *vetor;                                             // Aloca um vetor dinâmicamente para o teste.

    try {
            vetor = new int[10];
    } 
    catch (bad_alloc &ex) {
        cerr << ex.what () << " Erro de alocação dinâmica de memoria para o vetor!" << endl;
        exit (1);
    }
    catch (...) {
        cerr << " Erro de alocação de memoria desconhecido!" <<  endl;
        exit (1);
    }
    
    for(int ii = 0; ii < 10; ii++) {                        // Gera valores aleatórios para o vetor.
        vetor[ii] = rand() % 20;
    }

    cout << "Vetor que será utilizado de base para os testes de ordenação e de busca:" << endl;
    for(int ii = 0; ii < 10; ii++) {                        // Imprime o vetor.
        cout << vetor[ii] << " ";
    }
    cout << endl << endl;


    cout << "Ordenação com BUBBLE SORT:" << endl;
    BubbleSort (vetor, 10);                                 // Ordenação o vetor com o bubble sort:

    for(int ii = 0; ii < 10; ii++) {                        // Imprime o vetor.
        cout << vetor[ii] << " ";
    }
    cout << endl << endl;


    cout << "Ordenação com INSERTION SORT:" << endl; 
    InsertionSort (vetor, 10);                               // Ordenação do vetor com o insertion sort.
    
    for(int ii = 0; ii < 10; ii++) {                         // Imprime o vetor.
        cout << vetor[ii] << " ";
    }
    cout << endl << endl;


    cout << "Ordenação com MERGE SORT:" << endl;
    MergeSort (vetor, 0, 9);                                  // Ordenação do vetor com o merge sort.
    
    for(int ii = 0; ii < 10; ii++) {                          // Imprime o vetor.
        cout << vetor[ii] << " ";
    }
    cout << endl << endl;

   
    cout << "Ordenação com QUICK SORT:" << endl;
    QuickSort (vetor, 0, 9);                                   // Ordenação do vetor com o quick sort.
    
    for(int ii = 0; ii < 10; ii++) {                           // Imprime o vetor.
        cout << vetor[ii] << " ";
    }
    cout << endl << endl;


    cout << "Ordenação com SELECTION SORT:" << endl;
    SelectionSort (vetor, 10);                                 // Ordenação do vetor com o selection sort.
 
    for(int ii = 0; ii < 10; ii++) {                           // Imprime o vetor.
        cout << vetor[ii] << " ";
    }
    cout << endl << endl;

    // Testando buscas:
    cout << "Testando as buscas com o vetor já ordenado:" << endl << endl;

    cout << "Buscando o elemento '5', por meio da busca BINÁRIA ITERATIVA:" << endl;
    cout << BuscaBinaria_ite (vetor, 10, 5) << endl << endl;

    cout << "Buscando o elemento '7', por meio da busca BINÁRIA RECURSIVA:" << endl; 
    cout << BuscaBinaria_rec (vetor, 10, 7) << endl << endl;

    cout << "Buscando o elemento '1', por meio da busca SEQUÊNCIAL ITERATIVA:" << endl;     
    cout << BuscaSequencial_ite (vetor, 10, 1) << endl << endl;

    cout << "Buscando o elemento '3', por meio da busca SEQUÊNCIAL RECURSIVA:" << endl; 
    cout << BuscaSequencial_rec (vetor, 10, 3) << endl << endl;

    cout << "Buscando o elemento '10', por meio da busca TERNÁRIA ITERATIVA:" << endl;
    cout << BuscaTernaria_ite (vetor, 0, 10, 10) << endl << endl;

    cout << "Buscando o elemento '15', por meio da busca TERNÁRIA RECURSIVA:" << endl;
    cout << BuscaTernaria_rec (vetor, 0, 10, 15) << endl << endl;

    delete [] vetor;                                            // Liberando o espaço de memória.

    return 0;
}