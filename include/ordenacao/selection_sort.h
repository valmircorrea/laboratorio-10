/**
* @file     selection_sort.h
* @brief    Arquivo com a função de ordenação selection sort.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    19/06/17
* @date     20/06/17
*/

#ifndef SELECTION_SORT_H_
#define SELECTION_SORT_H_

namespace edb1 {

    /**
    * @brief Função que realiza a ordenação de um vetor.
    * @param *v Ponteiro genérico que aponta para um vetor.
    * @param N Variável que define o tamanho do vetor.
    */
    extern "C++" template <typename T>
    void SelectionSort (T *v, int N) {
        
        for (int ii = 0; ii < N; ii++ ) {

            int menor = ii;
            for (int jj = ii + 1; jj < N; jj++) {

                if (v[jj] < v[menor]) { 

                    int aux = v[ii]; 

                    menor = jj; 
                    v[ii] = v[menor]; 
                    v[menor] = aux; 
                }
            }
        }
    }
}

#endif