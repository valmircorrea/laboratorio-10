/**
* @file     bubble_sort.h
* @brief    Arquivo com a função de ordenação bubble sort.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    19/06/17
* @date     20/06/17
*/

#ifndef BUBBLE_SORT_H_
#define BUBBLE_SORT_H_

namespace edb1 {

    /**
    * @brief Função que realiza a ordenação de um vetor.
    * @param *v Ponteiro genérico que aponta para um vetor.
    * @param N Variável que define o tamanho do vetor.
    */
    extern "C++" template <typename T>
    void BubbleSort (T *v, int N) {

        int continuee;

        do {
            continuee = 0;

            for(int ii = 0; ii < N-1; ii++) {

                if (v[ii] > v[ii + 1]){

                    int aux = v[ii];
                    v[ii] = v[ii + 1];
                    v[ii + 1] = aux;
                    continuee = 1;
                    
                }

                
            }

        } while (continuee == 1);
    }
}

#endif