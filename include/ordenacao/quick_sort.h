/**
* @file   quick_sort.h
* @brief  Arquivo com a função de ordenação quick sort.
* @author Valmir Correa (valmircorrea96@outlook.com)
* @since  19/06/17
* @date   20/06/17
*/

#ifndef QUICK_SORT_H_
#define QUICK_SORT_H_

namespace edb1 {

    /**
    * @brief Função que realiza a ordenação de um vetor.
    * @param *v Ponteiro genérico que aponta para um vetor.
    * @param esq Variável que recebe o valor do inicio do vetor.
    * @param dir Variável que recebe o valor do final do vetor.
    */
    extern "C++" template <typename T>
    void QuickSort (T *v, int esq, int dir) {

        int aux;
        int i = esq;
        int j = dir;
        int meio = (esq + dir) / 2;
        int pivo = v[meio]; 

        do {

            while (v[i] < pivo ) {
                i++;
            }
            while (v[j] > pivo ) {
                j--;
            }

                aux = v[i];
                v[i] = v[j];
                v[j] = aux;

                i++;
                j--;

        } while (j > i);

        if (esq < j) {
            QuickSort (v, esq, j);
        }

        if (i < dir) {
            QuickSort (v, i, dir);
        }
    }
}

#endif