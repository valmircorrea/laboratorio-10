/**
* @file     insertion_sort.h
* @brief    Arquivo com a função de ordenação insert sort.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    19/06/17
* @date     20/06/17
*/

#ifndef INSERTION_SORT_H_
#define INSERTION_SORT_H_

namespace edb1 {

    /**
    * @brief Função que realiza a ordenação de um vetor.
    * @param *v Ponteiro genérico que aponta para um vetor.
    * @param N Variável que define o tamanho do vetor.
    */
    extern "C++" template <typename T>
    void InsertionSort (T *v, int N) {

        for (int ii = 1; ii < N; ii++) {

            for (int jj = ii; jj >= 1 && v[jj] < v[jj-1]; jj--) { 
                
                T aux = v[jj];
                
                v[jj] = v[jj-1]; 
                v[jj-1] = aux;
            }
        }
    }
}

#endif