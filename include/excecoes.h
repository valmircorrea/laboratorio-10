// /**
// * @file	    excecoes.h
// * @brief	Arquivo com as classes de tratamento de exceções.
// * @author   Valmir Correa (valmircorrea96@outlook.com)
// * @since	23/06/2017
// * @date     25/06/2017
// */

// #ifndef EXCECOES_H_
// #define EXCECOES_H_

// #include <exception>
// using std::exception;

// namespace edb1 {

//     class TamanhoInvalido : public exception {
//         private:
            
//             int tam;
//         public:
            
//             TamanhoInvalido (int n);
//             void Msg ();
//     };
    

// }

// #endif