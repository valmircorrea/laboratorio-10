/**
* @file     busca_binaria_ite.h
* @brief    Arquivo com a função de busca binária na forma iterativa.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    19/06/17
* @date     20/06/17
*/

#ifndef BUSCA_BINARIA_ITE_H_
#define BUSCA_BINARIA_ITE_H_

//#include "excecoes.h"

namespace edb1 {

    /**
    * @brief Função que realiza a busca binaria em um vetor.
    * @param *v Ponteiro genérico que aponta para um vetor.
    * @param N Variável que define o tamanho do vetor.
    * @param chave Variável que recebe a chave de busca a ser encontrada.
    * @return Retorna true ou false, se o elemento estiver no vetor ou não.
    */
    extern "C++" template <typename T>
    int BuscaBinaria_ite (T *v, int N, T chave) {

        // try {
        //     if (N <= 0) {

        //         throw ( TamanhoInvalido (N) );
        //     } 
        // } 
        // catch (TamanhoInvalido P) {
        //     P.Msg ();
        // }

        if (N <= 0) {

            return -1;
        } 

        int ini = 0;
        int fim = ini-1;
        int k = (ini+fim)/2;

        while (ini <= fim) {
            
            if (v[k] == chave) {
                return k;
            } 
            else if (v[k] < chave) {
                ini = k+1;
            }
            else {
                fim = k-1;
            }

            k = (ini+fim)/2;
        }

        return -1;
    }
}
#endif