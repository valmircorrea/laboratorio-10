/**
* @file     busca_sequencial_ite.h
* @brief    Arquivo com a função de busca sequêncial na forma iterativa.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    19/06/17
* @date     20/06/17
*/

#ifndef BUSCA_SEQUENCIAL_ITE_H_
#define BUSCA_SEQUENCIAL_ITE_H_

namespace edb1 {

    /**
    * @brief Função que realiza a busca sequencial em um vetor.
    * @param *v Ponteiro genérico que aponta para um vetor.
    * @param N Variável que define o tamanho do vetor.
    * @param chave Variável que recebe a chave de busca a ser encontrada.
    * @return Retorna true ou false, se o elemento estiver no vetor ou não.
    */
    extern "C++" template <typename T>
    int BuscaSequencial_ite (T *v, int N, T chave) {

        if (N <= 0) {

            return -1;
        } 

        for (int ii = 0; ii < N; ii++) {
            if (v[ii] == chave) {
                return ii;
            }
        }

        return -1;
    }
}

#endif