/**
* @file     busca_ternaria_rec.h
* @brief    Arquivo com a função de busca ternária.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    19/06/17
* @date     20/06/17
*/

#ifndef BUSCA_TERNARIA_REC_H_
#define BUSCA_TERNARIA_REC_H_

namespace edb1 {

    /**
    * @brief Realiza a busca ternaria de elemento no vetor.
    * @param *v Ponteiro genérico que aponta para um vetor.
    * @param ini Recebe o índice do inicio do vetor.
    * @param fim Recebe o índice do fim do vetor.
    * @param chave Recebe o elemento a ser buscado no vetor.
    * @return Retorna true ou false, se o elemento estiver no vetor ou não.
    */
    extern "C++" template <typename T>
    int BuscaTernaria_rec (T *v, int ini, int fim, T chave) {

        if (fim <= ini) {
            return -1;
        }

        int mid1 = ini + ((fim - ini)/3);
        int mid2 = ini + (2 *(fim - ini)/3);

        if (v[mid1] == chave) {
            return mid1;
        }
        else if (v[mid2] == chave) {
            return mid2;
        }
        else if (v[mid1] < chave) {
            return BuscaTernaria_rec (v,mid1+1, fim, chave);
        }
        else if (v[mid2] > chave) {
            return BuscaTernaria_rec (v, ini, mid2-1, chave);
        }
        else {
            return BuscaTernaria_rec (v, mid1+1, mid2-1, chave);
        }
        return -1;
    }
}

#endif