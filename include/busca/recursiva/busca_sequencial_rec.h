/**
* @file     busca_sequencial_rec.h
* @brief    Arquivo com a função de busca sequencial.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    19/06/17
* @date     20/06/17
*/

#ifndef BUSCA_SEQUENCIAL_REC_H_
#define BUSCA_SEQUENCIAL_REC_H_

namespace edb1 {

    /**
    * @brief Função que realiza a busca sequencial em um vetor.
    * @param *v Ponteiro genérico que aponta para um vetor.
    * @param N Variável que define o tamanho do vetor.
    * @param chave Variável que recebe a chave de busca a ser encontrada.
    * @return Retorna true ou false, se o elemento estiver no vetor ou não.
    */
    extern "C++" template <typename T>
    int BuscaSequencial_rec (T *v, int N, T chave) { 

        if (N <= 0) {

            return -1;
        }  

        if (v[0] == chave) {
            return 0;
        }

        else {
            return BuscaSequencial_rec (v+1, N - 1, chave);
        }

        return -1;
    }
}

#endif